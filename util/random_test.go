package util_test

import (
	"awesomeProject/util"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRandom(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	utils := util.NewMockIUtil(ctrl)
	utils.EXPECT().Random(gomock.Any()).Do(func(r util.Request) {
		assert.Equal(t, "testong2",r.S)
	}).Return("hello")
	assert.Equal(t, "hello", util.Random(utils))
}
